package com.mreil.ankidecks.thaialphabet;

import com.mreil.ankiconnectjava.AnkiRunner;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        new AnkiRunner().run();
    }
}
