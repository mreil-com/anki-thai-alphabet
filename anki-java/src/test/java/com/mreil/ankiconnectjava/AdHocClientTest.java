package com.mreil.ankiconnectjava;

import org.junit.Ignore;
import org.junit.Test;

@Ignore("run manually!")
public class AdHocClientTest {

    @Test
    public void test() {
        AnkiClient client = new AnkiClient();

        client.getDeckConfigResponse("Thai Alphabet");
        client.setDefaultUnlimitedNewCardsAndRandomOrder();
    }
}
