package com.mreil.ankiconnectjava.runtime;

import com.mreil.ankiconnectjava.AnkiClient;

import java.util.Optional;

public interface AnkiRuntime {
    int DEFAULT_PORT = 8765;
    String DEFAULT_HOST = "127.0.0.1";

    AnkiClient getClient();

    void shutdown();

    static AnkiRuntime get() {
        AnkiClient client = new AnkiClient();

        return remoteRuntime(client)
                .orElseGet(() -> localRuntime(client)
                        .orElseThrow(() -> new RuntimeException("no available runtime")));
    }

    static Optional<AnkiRuntime> remoteRuntime(AnkiClient client) {
        return client.ping()
                .map(ver -> new RemoteAnkiRuntime(client));
    }

    static Optional<AnkiRuntime> localRuntime(AnkiClient client) {
        try {
            LocalAnkiRuntime runtime = new LocalAnkiRuntime(client);
            return Optional.of(runtime);
        } catch (Exception e) {
            return Optional.empty();
        }
    }
}
