package com.mreil.ankiconnectjava.runtime;

import com.mreil.ankiconnectjava.AnkiClient;
import org.springframework.core.io.ClassPathResource;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.TimeoutRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.util.StreamUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class LocalAnkiRuntime extends AbstractAnkiRuntime {
    private final Process process;

    private RetryTemplate template = new RetryTemplate();

    LocalAnkiRuntime(AnkiClient client) {
        super(client);

        TimeoutRetryPolicy policy = new TimeoutRetryPolicy();
        policy.setTimeout(10000L);
        template.setRetryPolicy(policy);

        FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
        backOffPolicy.setBackOffPeriod(200L);
        template.setBackOffPolicy(backOffPolicy);

        try {
            this.process = startAnki();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        template.execute(ctx -> client.ping().orElseThrow(RuntimeException::new));
    }

    private Process startAnki() throws IOException {
        Path path = Files.createTempDirectory("anki");

        InputStream file = new ClassPathResource("prefs.zip").getInputStream();

        try (ZipInputStream zip = new ZipInputStream(file)) {
            ZipEntry entry;
            while ((entry = zip.getNextEntry()) != null) {
                Path filePath = path.resolve(entry.getName());

                if (entry.isDirectory()) {
                    filePath.toFile().mkdirs();
                    continue;
                }

                StreamUtils.copy(zip, new FileOutputStream(filePath.toFile()));
            }
        }

        Process p = Runtime.getRuntime().exec("anki -b " + path.toString());

        return p;
    }

    @Override
    public void shutdown() {
        try {
            process.destroyForcibly().waitFor();
        } catch (InterruptedException e) {
            // do nothing
        }
    }
}
