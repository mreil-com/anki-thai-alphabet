package com.mreil.ankiconnectjava.runtime;

import com.mreil.ankiconnectjava.AnkiClient;

public abstract class AbstractAnkiRuntime implements AnkiRuntime {
    protected final AnkiClient client;

    protected AbstractAnkiRuntime(AnkiClient client) {
        this.client = client;
    }

    @Override
    public AnkiClient getClient() {
        return client;
    }
}
