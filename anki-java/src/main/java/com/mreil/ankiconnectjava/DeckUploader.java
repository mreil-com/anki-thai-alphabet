package com.mreil.ankiconnectjava;

import com.google.common.reflect.ClassPath;
import com.mreil.ankiconnectjava.model.internal.DeckData;
import com.mreil.ankiconnectjava.util.ResourceReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class DeckUploader {
    private final Logger log = LoggerFactory.getLogger(DeckUploader.class);
    public static final String MODELS_DIR_NAME = "models";

    private final AnkiClient client;
    private final Path deckDirectory;
    private final DeckData deckData;
    private final Collection<Path> models;

    public DeckUploader(AnkiClient client, Path deckDirectory) throws IOException {
        this.client = client;
        this.deckDirectory = deckDirectory;
        this.deckData = DeckData.fromCsv(deckDirectory.resolve("data.csv").toString());

        List<ClassPath.ResourceInfo> allResources = getResources();

        Path modelsPath = deckDirectory.resolve(MODELS_DIR_NAME);
        models = allResources.stream()
                .map(res -> Paths.get(res.getResourceName()))
                .filter(path -> path.startsWith(modelsPath))
                .map(Path::getParent)
                .filter(path -> path.getParent().equals(modelsPath))
                .collect(Collectors.toSet());
        if (models.isEmpty()) {
            log.error("No models found at '{}'", modelsPath);
        }
    }

    public String upload() throws IOException {
        String deckName = deckDirectory.getFileName().toString();
        log.info("Uploading deck '{}'...", deckName);
        client.createDeck(deckName);

        for (Path model : models) {
            String modelName = new ModelUploader(client, model, deckData).upload();
            new DataUploader(deckName, client, modelName, deckData).upload();
        }

        return deckName;
    }


    public List<ClassPath.ResourceInfo> getResources() throws IOException {
        List<ClassPath.ResourceInfo> allResources = new ResourceReader(deckDirectory.toString()).getAllResources();
        if (allResources.isEmpty()) {
            log.error("Deck directory for Anki deck '{}' not found.", deckDirectory);
        }
        return allResources;
    }
}
