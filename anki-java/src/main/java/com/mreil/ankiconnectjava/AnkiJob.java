package com.mreil.ankiconnectjava;

import java.io.IOException;

public interface AnkiJob {
    void run(AnkiClient client) throws IOException;
}
