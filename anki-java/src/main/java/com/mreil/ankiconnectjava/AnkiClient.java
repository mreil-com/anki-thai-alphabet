package com.mreil.ankiconnectjava;

import com.mreil.ankiconnectjava.model.AddNoteAction;
import com.mreil.ankiconnectjava.model.BooleanListResponse;
import com.mreil.ankiconnectjava.model.BooleanResponse;
import com.mreil.ankiconnectjava.model.CreateDeckAction;
import com.mreil.ankiconnectjava.model.CreateModelAction;
import com.mreil.ankiconnectjava.model.CreateModelResponse;
import com.mreil.ankiconnectjava.model.DeckConfig;
import com.mreil.ankiconnectjava.model.DeckConfigResponse;
import com.mreil.ankiconnectjava.model.DeleteDeckAction;
import com.mreil.ankiconnectjava.model.DeleteModelAction;
import com.mreil.ankiconnectjava.model.IdResponse;
import com.mreil.ankiconnectjava.model.ImmutableCreateDeckAction;
import com.mreil.ankiconnectjava.model.ImmutableDeckConfig;
import com.mreil.ankiconnectjava.model.ImmutableDeleteDeckAction;
import com.mreil.ankiconnectjava.model.ImmutableDeleteModelAction;
import com.mreil.ankiconnectjava.model.ImmutableExportPackageAction;
import com.mreil.ankiconnectjava.model.ImmutableGetDeckConfigAction;
import com.mreil.ankiconnectjava.model.ImmutableIdResponse;
import com.mreil.ankiconnectjava.model.ImmutableSaveDeckConfigAction;
import com.mreil.ankiconnectjava.model.ImmutableStoreMediaFileAction;
import com.mreil.ankiconnectjava.model.ImmutableVersionAction;
import com.mreil.ankiconnectjava.model.MediaFile;
import com.mreil.ankiconnectjava.model.Response;
import com.mreil.ankiconnectjava.model.StoreMediaFileAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.http.codec.DecoderHttpMessageReader;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.netty.http.client.HttpClient;

import java.time.Duration;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

public class AnkiClient {
    private final Logger log = LoggerFactory.getLogger(AnkiClient.class);
    private final WebClient client;

    public AnkiClient() {
        HttpClient httpClient = HttpClient
                .create()
                .wiretap(true);

        client = WebClient
                .builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .baseUrl("http://localhost:8765")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .codecs(clientCodecConfigurer -> {
                    DecoderHttpMessageReader<Object> dec = new DecoderHttpMessageReader<>(
                            new Jackson2JsonDecoder(
                                    Jackson2ObjectMapperBuilder.json()
                                            .build(), MediaType.parseMediaType("text/json")));
                    clientCodecConfigurer.customCodecs().register(dec);
                })
                .build();

    }

    public long createDeck(String name) {
        IdResponse res =
                deleteDeck(name, client)
                        .then(createDeck(name, client))
                        .block();

        return res.getResult().orElseThrow(IllegalArgumentException::new);
    }

    public IdResponse addNote(AddNoteAction addNoteAction) {
        return addNote(client, addNoteAction)
                .retry(5)
                .block();
    }

    public CreateModelResponse createModel(CreateModelAction createModelAction) {
        return createModel(client, createModelAction)
                .block();
    }

    public void deleteModel(String modelName) {
        deleteModel(modelName, client)
                .block();
    }

    public Boolean storeMediaFiles(Collection<MediaFile> mediaFiles) {
        return Flux.fromIterable(mediaFiles)
                .parallel()
                .runOn(Schedulers.elastic())
                .map(this::storeMediaFile)
                .ordered((r1, r2) -> 1)
                .all(Objects::nonNull)
                .doOnSuccess(t -> log.info("All media files stored successfully."))
                .block();
    }

    private IdResponse storeMediaFile(MediaFile mediaFile) {
        log.info("Storing media file: '{}' ...", mediaFile);
        return storeMediaFile(client, ImmutableStoreMediaFileAction.builder()
                .params(ImmutableStoreMediaFileAction.Params.builder()
                        .filename(mediaFile.getName())
                        .data(mediaFile.getBase64Data())
                        .build())
                .build())
                .block();
    }

    public Optional<Long> version() {
        return version(client)
                .blockOptional()
                .flatMap(Response::getResult);
    }

    public Optional<Long> ping() {
        return version(client)
                .doOnError(err -> log.error("Cannot connect to running Anki..."))
                .onErrorResume(err -> Mono.just(ImmutableIdResponse.builder().build()))
                .blockOptional()
                .flatMap(Response::getResult);
    }

    public boolean export(String deck, String path) {
        BooleanListResponse response = export(client, deck, path)
                .blockOptional()
                .orElseThrow(RuntimeException::new);

        response.getError().ifPresent(err -> {
            log.error("Error exporting package: {}", err);
        });

        return response.getResult()
                .orElseThrow(RuntimeException::new)
                .stream()
                .findFirst()
                .orElseThrow(RuntimeException::new);
    }

    private Mono<IdResponse> deleteDeck(String name, WebClient client) {
        DeleteDeckAction deleteDeckAction = ImmutableDeleteDeckAction.builder()
                .params(ImmutableDeleteDeckAction.DeckParams.builder()
                        .addDecks(name)
                        .build())
                .build();

        return client.post()
                .body(BodyInserters.fromValue(deleteDeckAction))
                .exchange()
                .flatMap(res -> res.bodyToMono(IdResponse.class));
    }

    private Mono<IdResponse> createDeck(String name, WebClient client) {
        CreateDeckAction createDeckAction = ImmutableCreateDeckAction.builder()
                .params(ImmutableCreateDeckAction.DeckParams.builder()
                        .deck(name)
                        .build())
                .build();

        return client.post()
                .body(BodyInserters.fromValue(createDeckAction))
                .exchange()
                .flatMap(res -> res.bodyToMono(IdResponse.class));
    }

    private Mono<IdResponse> deleteModel(String name, WebClient client) {
        DeleteModelAction deleteModelAction = ImmutableDeleteModelAction.builder()
                .params(ImmutableDeleteModelAction.Params.builder()
                        .addModels(name)
                        .build())
                .build();

        return client.post()
                .body(BodyInserters.fromValue(deleteModelAction))
                .exchange()
                .doOnError(t -> {
                    log.error("ERRORR", t);
                })
                .flatMap(res -> res.bodyToMono(IdResponse.class));
    }

    private Mono<CreateModelResponse> createModel(WebClient client, CreateModelAction createModelAction) {
        return client.post()
                .body(BodyInserters.fromValue(createModelAction))
                .exchange()
                .flatMap(res -> res.bodyToMono(CreateModelResponse.class));
    }

    private Mono<IdResponse> addNote(WebClient client, AddNoteAction addNoteAction) {
        return client.post()
                .body(BodyInserters.fromValue(addNoteAction))
                .exchange()
                .retry(5)
                .timeout(Duration.ofSeconds(20))
                .doFirst(() -> log.info("Storing note: " + addNoteAction.getParams().getNote().getFields()))
                .doOnError(e -> log.error("Error storing note: " + addNoteAction.getParams().getNote(), e))
                .doOnSuccess(e -> log.info("Stored note."))
                .flatMap(res -> res.bodyToMono(IdResponse.class));
    }

    private Mono<IdResponse> storeMediaFile(WebClient client, StoreMediaFileAction action) {
        return client.post()
                .body(BodyInserters.fromValue(action))
                .retrieve()
                .bodyToMono(IdResponse.class)
                .doOnError(e -> log.error("Error storing: " + action.getParams().getFilename(), e))
                .doOnSuccess(e -> log.info("Stored: " + action.getParams().getFilename(), e))
                .timeout(Duration.ofSeconds(20))
                .retry(5);
    }

    private Mono<IdResponse> version(WebClient client) {
        return client.post()
                .body(BodyInserters.fromValue(ImmutableVersionAction.builder().build()))
                .retrieve()
                .bodyToMono(IdResponse.class);
    }

    private Mono<BooleanListResponse> export(WebClient client,
                                             String deckName,
                                             String path) {
        return client.post()
                .body(BodyInserters.fromValue(ImmutableExportPackageAction.builder()
                        .params(
                                ImmutableExportPackageAction.Params.builder()
                                        .deck(deckName)
                                        .path(path)
                                        .includeSched(true)
                                        .build()
                        )
                        .build()))
                .exchange()
                .doOnError(err -> log.error("Error exporting deck: {}", err, err))
                .flatMap(res -> res.bodyToMono(BooleanListResponse.class));
    }

    public DeckConfig getDeckConfigResponse(String deckName) {
        return getDeckConfig(client, deckName)
                .blockOptional()
                .map(Response::getResult)
                .orElseThrow(RuntimeException::new)
                .orElseThrow(RuntimeException::new);
    }

    private Mono<DeckConfigResponse> getDeckConfig(WebClient client,
                                                   String deckName) {
        return client.post()
                .body(BodyInserters.fromValue(ImmutableGetDeckConfigAction.builder()
                        .params(
                                ImmutableGetDeckConfigAction.Params.builder()
                                        .deck(deckName)
                                        .build()
                        )
                        .build()))
                .exchange()
                .flatMap(res -> res.bodyToMono(DeckConfigResponse.class));
    }

    public boolean setDefaultUnlimitedNewCardsAndRandomOrder() {
        DeckConfig.New unlimitedNew = ImmutableDeckConfig.New.builder()
                .perDay(9999)
                .order(0)
                .build();
        return setDeckConfig(ImmutableDeckConfig.builder()
                .id(1)
                .name("Default")
                .getNew(unlimitedNew)
                .build())
                .getResult()
                .orElseThrow(RuntimeException::new);
    }

    public BooleanResponse setDeckConfig(DeckConfig deckConfig) {
        return setDeckConfig(client, deckConfig)
                .block();
    }

    private Mono<BooleanResponse> setDeckConfig(WebClient client,
                                                DeckConfig deckConfig) {
        return client.post()
                .body(BodyInserters.fromValue(ImmutableSaveDeckConfigAction.builder()
                        .params(
                                ImmutableSaveDeckConfigAction.Params.builder()
                                        .config(deckConfig)
                                        .build()
                        )
                        .build()))
                .exchange()
                .flatMap(res -> res.bodyToMono(BooleanResponse.class));
    }
}
