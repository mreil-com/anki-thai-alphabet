package com.mreil.ankiconnectjava.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
@Value.Enclosing
public abstract class CreateModelAction implements ParamsAction<CreateModelAction.Params> {
    @Override
    public String getAction() {
        return "createModel";
    }

    @Value.Immutable
    public interface Params extends DefaultParams {
        abstract class Builder implements ParamsBuilder {
        }

        String getModelName();

        List<String> getInOrderFields();

        String getCss();

        List<CardTemplates> getCardTemplates();
    }

    @Value.Immutable
    public interface CardTemplates {
        @JsonProperty("Front")
        String getFront();

        @JsonProperty("Back")
        String getBack();
    }
}
