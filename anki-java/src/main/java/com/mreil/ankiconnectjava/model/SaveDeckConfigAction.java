package com.mreil.ankiconnectjava.model;

import org.immutables.value.Value;

/**
 * https://docs.ankiweb.net/#/deck-options
 */
@Value.Immutable
@Value.Enclosing
public interface SaveDeckConfigAction extends ParamsAction<SaveDeckConfigAction.Params> {

    @Override
    default String getAction() {
        return "saveDeckConfig";
    }

    /**
     * <pre>
     * "params": {
     *    "config": {
     *        "lapse": {
     *        "leechFails": 8,
     *        "delays": [10],
     *        "minInt": 1,
     *        "leechAction": 0,
     *        "mult": 0
     *    },
     *    "dyn": false,
     *    "autoplay": true,
     *    "mod": 1502970872,
     *    "id": 1,
     *    "maxTaken": 60,
     *    "new": {
     *        "bury": true,
     *        "order": 1,
     *        "initialFactor": 2500,
     *        "perDay": 20,
     *        "delays": [1, 10],
     *        "separate": true,
     *        "ints": [1, 4, 7]
     *    },
     *    "name": "Default",
     *    "rev": {
     *        "bury": true,
     *        "ivlFct": 1,
     *        "ease4": 1.3,
     *        "maxIvl": 36500,
     *        "perDay": 100,
     *        "minSpace": 1,
     *        "fuzz": 0.05
     *     },
     *     "timer": 0,
     *     "replayq": true,
     *     "usn": -1
     * }
     * </pre>
     */
    @Value.Immutable
    interface Params extends DefaultParams {
        abstract class Builder implements ParamsBuilder {
        }

        DeckConfig getConfig();
    }
}
