package com.mreil.ankiconnectjava.model;

import org.immutables.value.Value;

import java.util.List;
import java.util.Map;

@Value.Immutable
@Value.Enclosing
public interface Note {
    String getDeckName();

    String getModelName();

    List<String> getTags();

    Map<String, String> getFields();

    Map<String, Object> getOptions();

    Void getAudio();
}
