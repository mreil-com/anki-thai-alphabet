package com.mreil.ankiconnectjava.model;

import org.immutables.value.Value;

@Value.Immutable
@Value.Enclosing
public interface GetDeckConfigAction extends ParamsAction<GetDeckConfigAction.Params> {

    @Override
    default String getAction() {
        return "getDeckConfig";
    }

    @Value.Immutable
    interface Params extends DefaultParams {
        abstract class Builder implements ParamsBuilder {
        }

        String getDeck();
    }
}
