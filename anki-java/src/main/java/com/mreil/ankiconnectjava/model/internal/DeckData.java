package com.mreil.ankiconnectjava.model.internal;

import com.mreil.ankiconnectjava.model.MediaFile;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Set;

public interface DeckData {
    static DeckData fromCsv(String file) throws IOException {
        return CsvDataImpl.fromFile(file);
    }

    Set<String> getFieldNames();

    Collection<DataRecord> getRecords();

    Collection<MediaFile> getAllMediaFiles(Path rootDir);
}
