package com.mreil.ankiconnectjava.model;

import org.immutables.value.Value;

public interface Action {
    String getAction();

    @Value.Default
    default int getVersion() {
        return 6;
    }
}
