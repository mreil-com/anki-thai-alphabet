package com.mreil.ankiconnectjava.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
@JsonDeserialize(builder = ImmutableBooleanListResponse.Builder.class)
public interface BooleanListResponse extends Response<List<Boolean>> {
}
