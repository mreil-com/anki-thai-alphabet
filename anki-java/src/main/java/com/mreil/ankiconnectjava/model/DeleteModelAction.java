package com.mreil.ankiconnectjava.model;

import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
@Value.Enclosing
public interface DeleteModelAction extends ParamsAction<DeleteModelAction.Params> {

    @Override
    default String getAction() {
        return "deleteModel";
    }

    @Value.Immutable
    interface Params extends DefaultParams {
        abstract class Builder implements ParamsBuilder {
        }

        List<String> getModels();
    }
}
