package com.mreil.ankiconnectjava.model;

import org.immutables.value.Value;

@Value.Immutable
@Value.Enclosing
public interface VersionAction extends Action {

    @Override
    default String getAction() {
        return "version";
    }

}
