package com.mreil.ankiconnectjava.model;

public interface ParamsAction<T extends DefaultParams> extends Action {
    T getParams();
}
