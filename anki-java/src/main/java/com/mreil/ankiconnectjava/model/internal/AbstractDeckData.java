package com.mreil.ankiconnectjava.model.internal;

import com.mreil.ankiconnectjava.model.ImmutableMediaFile;
import com.mreil.ankiconnectjava.model.MediaFile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Base64;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class AbstractDeckData implements DeckData {
    protected final Collection<DataRecord> records;
    private final Set<String> fieldNames;

    protected AbstractDeckData(Collection<DataRecord> records,
                               Set<String> fieldNames) {
        this.fieldNames = fieldNames;
        this.records = records;

    }

    @Override
    public Collection<DataRecord> getRecords() {
        return records;
    }

    @Override
    public Set<String> getFieldNames() {
        return fieldNames;
    }

    @Override
    public Collection<MediaFile> getAllMediaFiles(Path rootDir) {
        return records.stream()
                .map(DataRecord::getMediaFileNames)
                .flatMap(Set::stream)
                .map(name -> ImmutableMediaFile.builder()
                        .name(name)
                        .base64Data(readData(rootDir, name))
                        .build())
                .collect(Collectors.toList());
    }

    private String readData(Path rootDir, String name) {
        try {
            Path path = rootDir.resolve(name);
            ClassPathResource file = new ClassPathResource(path.toString());
            byte[] bytes = StreamUtils.copyToByteArray(file.getInputStream());
            return Base64.getEncoder().encodeToString(bytes);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
