package com.mreil.ankiconnectjava.model;

import java.util.Optional;

public interface Response<T> {
    Optional<String> getError();

    Optional<T> getResult();
}
