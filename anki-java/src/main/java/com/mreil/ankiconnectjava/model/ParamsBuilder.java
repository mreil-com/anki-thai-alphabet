package com.mreil.ankiconnectjava.model;

public interface ParamsBuilder {
    DefaultParams build();
}
