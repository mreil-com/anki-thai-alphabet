package com.mreil.ankiconnectjava.model;

import org.immutables.value.Value;

@Value.Immutable
@Value.Enclosing
public interface StoreMediaFileAction extends ParamsAction<StoreMediaFileAction.Params> {

    @Override
    default String getAction() {
        return "storeMediaFile";
    }

    @Value.Immutable
    interface Params extends DefaultParams {
        abstract class Builder implements ParamsBuilder {
        }

        String getFilename();

        String getData();
    }
}
