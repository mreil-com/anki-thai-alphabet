package com.mreil.ankiconnectjava.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonDeserialize(builder = ImmutableDeckConfigResponse.Builder.class)
public interface DeckConfigResponse extends Response<DeckConfig> {
}
