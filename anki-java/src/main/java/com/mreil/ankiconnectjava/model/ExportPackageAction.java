package com.mreil.ankiconnectjava.model;

import org.immutables.value.Value;

import java.util.Optional;

@Value.Immutable
@Value.Enclosing
public interface ExportPackageAction extends ParamsAction<ExportPackageAction.Params> {

    @Override
    default String getAction() {
        return "exportPackage";
    }

    @Value.Immutable
    interface Params extends DefaultParams {
        abstract class Builder implements ParamsBuilder {
        }

        String getDeck();

        String getPath();

        Optional<Boolean> getIncludeSched();
    }
}
