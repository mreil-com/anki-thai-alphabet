package com.mreil.ankiconnectjava.model;

import org.immutables.value.Value;

import java.util.List;
import java.util.Map;

@Value.Immutable
@Value.Enclosing
public abstract class AddNoteAction implements ParamsAction<AddNoteAction.Params> {
    @Override
    public String getAction() {
        return "addNote";
    }

    @Value.Immutable
    public interface Params extends DefaultParams {
        abstract class Builder implements ParamsBuilder {
        }

        Note getNote();
    }

    @Value.Immutable
    public interface Note {
        String getDeckName();

        String getModelName();

        Map<String, String> getFields();

        List<String> getTags();
    }

    @Value.Immutable
    public interface Options extends DefaultParams {
        abstract class Builder implements ParamsBuilder {
        }

        boolean isAllowDuplicate();
    }
}
