package com.mreil.ankiconnectjava.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonDeserialize(builder = ImmutableIdResponse.Builder.class)
public interface IdResponse extends Response<Long> {
}
