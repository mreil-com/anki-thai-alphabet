package com.mreil.ankiconnectjava.model;

import org.immutables.value.Value;

@Value.Immutable
public interface MediaFile {
    String getName();

    @Value.Redacted
    String getBase64Data();
}
