package com.mreil.ankiconnectjava;

import com.mreil.ankiconnectjava.model.AddNoteAction;
import com.mreil.ankiconnectjava.model.ImmutableAddNoteAction;
import com.mreil.ankiconnectjava.model.internal.DataRecord;
import com.mreil.ankiconnectjava.model.internal.DeckData;

public class DataUploader {
    private final String deckName;
    private final AnkiClient client;
    private final String modelName;
    private final DeckData deckData;

    public DataUploader(String deckName, AnkiClient client, String modelName, DeckData deckData) {
        this.deckName = deckName;
        this.client = client;
        this.modelName = modelName;
        this.deckData = deckData;
    }

    public void upload() {
        for (DataRecord record : deckData.getRecords()) {
            addNote(deckName, modelName, client, record);
        }
    }

    private void addNote(String deckName, String modelName, AnkiClient client, DataRecord record) {
        ImmutableAddNoteAction.Note.Builder note = ImmutableAddNoteAction.Note.builder()
                .deckName(deckName)
                .modelName(modelName)
                .putAllFields(record.getFields())
                .addAllTags(record.getTags());

        AddNoteAction action = ImmutableAddNoteAction.builder()
                .params(ImmutableAddNoteAction.Params.builder()
                        .note(note.build())
                        .build())
                .build();
        client.addNote(action);
    }
}
